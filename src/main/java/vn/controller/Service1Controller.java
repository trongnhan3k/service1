package vn.controller;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.base.BaseController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by nhanvt on 2020/07/08
 */
@RestController
@RequestMapping(value = "/service1", method = RequestMethod.GET)
public class Service1Controller extends BaseController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String GETMedia() {
        return "Hello";
    }

    @PostMapping(value = "/pub")
    public ResponseEntity<String> publish(@RequestBody vn.input.RequestBody data) {
        if (data.getText().isEmpty() || data.getText().equals("")) {
            return new ResponseEntity<>("No data", HttpStatus.BAD_REQUEST);
        }

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.66.125");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare("test", "fanout");

            //publish to topic test
            String message = data.getText();
            channel.basicPublish("test", "", null, message.getBytes());
            logger.info(" [x] Message has been sent : " + message);

            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (IOException | TimeoutException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>("failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
